var gulp = require('gulp');
var jscrambler = require('gulp-jscrambler');
 
gulp.task('default', function (done) {
  gulp
    .src(['./**/*.js', '!./node_modules/**', '!./gulpfile.js'])
    .pipe(jscrambler({
      keys: {
        accessKey: '56750C6C1280B1CB74C06079E53F40411F801D3B',
        secretKey: 'BB8B13A159B7672BB18B0B047A4DC7F5782140DC'
      },
      applicationId: '5c034c686c4d7c001c742165',
      params: [
        {
          "name": "whitespaceRemoval"
        },
        {
          "name": "dotToBracketNotation"
        },
        {
          "name": "stringConcealing"
        },
        {
          "name": "extendPredicates"
        },
        {
          "name": "functionReordering"
        },
        {
          "options": {
            "features": [
              "opaqueFunctions"
            ]
          },
          "name": "functionOutlining"
        },
        {
          "name": "propertyKeysObfuscation"
        },
        {
          "name": "regexObfuscation"
        },
        {
          "options": {
            "features": [
              "opaqueSteps",
              "deadClones"
            ]
          },
          "name": "controlFlowFlattening"
        },
        {
          "name": "booleanToAnything"
        },
        {
          "name": "identifiersRenaming"
        }
      ]
    }))
    .pipe(gulp.dest('.'))
    .on('end', done);
});
